﻿using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Tools.Ribbon;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
//using InteropExcel = Microsoft.Office.Interop.Excel;

//using Microsoft.Office.Interop.Excel;
//using Excel = Microsoft.Office.Interop.Excel;
//using System.IO;

namespace ExcelAddIn.LogWork
{
    public partial class LogWorkRibbon
    {
        // https://stackoverflow.com/questions/2522243/vsto-excel-2007-include-or-embed-a-workbook-worksheet-in-an-add-in

        private void LogWork_Load(object sender, RibbonUIEventArgs e)
        {
            //Microsoft.Office.Interop.Excel.Application app = Globals.ThisAddIn.Application;
            //Workbook workbook = app.ActiveWorkbook;
            //app.default
            // Environment.CurrentDirectory
        }

        private void BtnExportReport_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                Workbook sourceWorkbook = (Workbook)Globals.ThisAddIn.Application.ActiveWorkbook;
                Worksheet sourceWorksheet = (Worksheet)Globals.ThisAddIn.Application.ActiveSheet;

                // poročiloDelaInUr_KobiLabs_MESEC_LETO_AlešKoncilja.xlsx
                DateTime lastMonth = DateTime.Now.Date.AddMonths(-1);
                string companyName;
                if (sourceWorkbook.Name.ToLower().Contains("kobi"))
                {
                    companyName = "KobiLabs";
                }
                else if (sourceWorkbook.Name.ToLower().Contains("cgs"))
                {
                    companyName = "CGSLabs";
                }
                else
                {
                    companyName = "no-name";
                }
                //string.Format("poročiloDelaInUr - {0} - {1}_{2}_AlešKoncilja.xlsx", companyName, Globals.ThisAddIn.MonthsSi[lastMonth.Month].ToLower(), lastMonth.Year);
                string filename = string.Format("poročiloDelaInUr - {0} - {1}_AlešKoncilja.xlsx", companyName, sourceWorksheet.Name.Replace(" ", "_"));

                Workbook reportWorkbook = Globals.ThisAddIn.Application.Workbooks.Add();
                Worksheet reportWorksheet = (Worksheet)reportWorkbook.ActiveSheet;

                // create temporary template and open with excel
                string sPath = Path.GetTempFileName();
                File.WriteAllBytes(sPath, Properties.Resources.report_template);
                Workbook templateWorkbook = Globals.ThisAddIn.Application.Workbooks.Open(sPath);

                // specify worksheet from a different workbook copies the first worksheet into destination workbook
                Worksheet templateWorksheet = (Worksheet)templateWorkbook.ActiveSheet;
                templateWorksheet.Copy(After: reportWorksheet);

                // close template and delete it
                templateWorkbook.Close(false);
                File.Delete(sPath);

                Worksheet reportWS = (Worksheet)reportWorkbook.Worksheets["Work report"];
                ((Worksheet)reportWorkbook.Worksheets["Sheet1"]).Delete();
                reportWS.Name = sourceWorksheet.Name;

                if (companyName.Equals("CGSLabs"))
                {
                    ((Range)reportWS.Cells[37, 8]).Value2 = "ADD DIAGRAM TO SHOW PROPORTION WORK HOUR BY PROJECTS!!!";
                }
                else if (companyName.Equals("no-name"))
                {
                    ((Range)reportWS.Cells[3, 10]).Value2 = "COMPANY IS NOT DETECTED!!!";
                }

                // write data
                Stopwatch sw = Stopwatch.StartNew();
                /*List<string> sourceColumns = new List<string>() { "B", "C", "D", "G" };
                List<string> reportColumns = new List<string>() { "C", "D", "E", "H" };
                // source rows: 7 - 37
                // report rows: 4 - 34
                for (int i = 0; i < sourceColumns.Count; i++)
                {
                    for (int j = 4; j <= 34; j++)
                    {
                        //reportWS.get_Range(string.Format("{0}{1}", reportColumns[i], j)).Value2 = sourceWorksheet.get_Range(string.Format("{0}{1}", sourceColumns[i], j + 3)).Value2;
                        //var rep = ((Range)reportWS.Cells[j, i + 3]).Value2;
                        //var sou = ((Range)sourceWorksheet.Cells[j + 3, i + 2]).Value2;
                        ((Range)reportWS.Cells[j, i + 3]).Value2 = ((Range)sourceWorksheet.Cells[j + 3, i + 2]).Value2;
                    }
                }*/
                reportWS.get_Range("C4", "E34").Value2 = sourceWorksheet.get_Range("B7", "D37").Value2;
                reportWS.get_Range("H4", "H34").Value2 = sourceWorksheet.get_Range("G7", "G37").Value2;

                long ms = sw.ElapsedMilliseconds;
                Debug.WriteLine(ms);

                // Save report
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Excel Workbook|*.xlsx";
                saveFileDialog.Title = "Save an Report File";
                saveFileDialog.FileName = filename;
                //saveFileDialog.OverwritePrompt = false;
                DialogResult dialogResult = saveFileDialog.ShowDialog();
                if (!string.IsNullOrEmpty(saveFileDialog.FileName) && dialogResult == DialogResult.OK)
                {
                    reportWorkbook.SaveCopyAs(saveFileDialog.FileName);
                    reportWorkbook.Saved = true;
                    reportWorkbook.Close();
                    //reportWorkbook.SaveAs(saveFileDialog.FileName);

                    if (MessageBox.Show("Do you want open report?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        reportWorkbook = Globals.ThisAddIn.Application.Workbooks.Open(saveFileDialog.FileName);
                    }

                    if (companyName.Equals("CGSLabs"))
                    {
                        MessageBox.Show("Add diagram, because report is for CGS Labs!", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}