﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Excel;

namespace ExcelAddIn.LogWork
{
    public partial class ThisAddIn
    {
        public Dictionary<int, string> MonthsSi = new Dictionary<int, string>()
        {
            {1, "Januar" },
            {2, "Februar" },
            {3, "Marec" },
            {4, "April" },
            {5, "Maj" },
            {6, "Junij" },
            {7, "Julij" },
            {8, "Avgust" },
            {9, "September" },
            {10, "Oktober" },
            {11, "November" },
            {12, "December" }
        };

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }

        #endregion
    }
}
